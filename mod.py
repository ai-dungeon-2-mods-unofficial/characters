from modloader import BaseMod

class Characters(BaseMod):
    def game_start(self, generator, story_manager):
        self.current_character = 'You'

    def preprocess_action(self, action):
        return (f'> {self.current_character} {action}', False)
    
    def try_handle_command(self, command, args, generator, story_manager):
        if command == 'setchar':
            self.current_character = ' '.join(args)
            print(f'Set character to {self.current_character}')
            return True
        return False
    
    def instructions(self):
        return ['\n  "/setchar"  Set your current character. You must speak in third person.']
